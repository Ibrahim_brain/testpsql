package com.itbrain229.testpsql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestpsqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestpsqlApplication.class, args);
	}

}
