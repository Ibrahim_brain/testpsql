package com.itbrain229.testpsql.repository;

import com.itbrain229.testpsql.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeRepositry extends JpaRepository<Employee, Long> {
}
