package com.itbrain229.testpsql.controller;

import com.itbrain229.testpsql.exception.ResourceNotFoundException;
import com.itbrain229.testpsql.model.Employee;
import com.itbrain229.testpsql.repository.EmployeRepositry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "employees", description = "the Employee API")
public class EmployeController {

    @Autowired
    private  EmployeRepositry employeRepositry;

    @Operation(summary = "Find All employees", description = "search all employees", tags = { "employees" })
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content = @Content(
                            array = @ArraySchema(schema = @Schema(implementation = Employee.class))))})
    @GetMapping(value = "/employees",produces = { "application/json", "application/xml" })
    public ResponseEntity<List<Employee>> getAll(){
        return  new ResponseEntity<>(employeRepositry.findAll(), HttpStatus.FOUND);
    }

    @Operation(summary = "Find employee by ID", description = "Return a single employee", tags = {"employees"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "successful operation",
            content = @Content(schema = @Schema(implementation = Employee.class))),
            @ApiResponse(responseCode = "401", description = "contact not found")
    })
    @GetMapping(value = "/employees/{id}", produces = {"application/json"})
    public ResponseEntity<Employee> getById(
            @Parameter(description = "Id of the employee to be obtain. Couldnot be empty", required = true) @PathVariable(value = "id") Long employeeId) throws ResourceNotFoundException {
            Employee employee = employeRepositry.findById(employeeId)
                    .orElseThrow(()-> new ResourceNotFoundException("Employee not found for this id :: "+ employeeId));
        return ResponseEntity.ok().body(employee);
    }

    @Operation(summary = "Create a new employee", description = "create a new employee with some basic information", tags = {"employees"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Employee.class))),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Employee already exist")
    })
    @PostMapping(value = "/employees", consumes = {"application/json"})
    public Employee createEmp(
            @Parameter(description = "Employee to create, can cot be null", required = true,
            schema = @Schema(implementation = Employee.class))
            @Valid @RequestBody Employee employee){
        return employeRepositry.save(employee);
    }

    @Operation(summary = "Update an existing employee", description = "", tags = { "employees" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
            @ApiResponse(responseCode = "404", description = "Contact not found"),
            @ApiResponse(responseCode = "405", description = "Validation exception") })
    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(
            @Parameter(description = "employee ID to update", required = true)
            @PathVariable(value = ("id")) Long employeId ,
            @Parameter(description = "Employee to update ",
                    required = true,
                    schema = @Schema(implementation = Employee.class))
            @Valid @RequestBody Employee employee)
        throws ResourceNotFoundException{
        Employee employee1 = employeRepositry.findById(employeId)
                .orElseThrow(()-> new ResourceNotFoundException("employees not found for this Id::"+ employeId));
        employee1.setEmailId(employee.getEmailId());
        employee1.setFirstName(employee.getFirstName());
        employee1.setLastName(employee.getLastName());
        final Employee employeeUp = employeRepositry.save(employee1);
        return ResponseEntity.ok(employeeUp);
    }

    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
            throws ResourceNotFoundException {
        Employee employee = employeRepositry.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
        employeRepositry.delete(employee);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
